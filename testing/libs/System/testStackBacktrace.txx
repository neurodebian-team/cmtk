/*
//
//  Copyright 1997-2009 Torsten Rohlfing
//
//  Copyright 2004-2011 SRI International
//
//  This file is part of the Computational Morphometry Toolkit.
//
//  http://www.nitrc.org/projects/cmtk/
//
//  The Computational Morphometry Toolkit is free software: you can
//  redistribute it and/or modify it under the terms of the GNU General Public
//  License as published by the Free Software Foundation, either version 3 of
//  the License, or (at your option) any later version.
//
//  The Computational Morphometry Toolkit is distributed in the hope that it
//  will be useful, but WITHOUT ANY WARRANTY; without even the implied
//  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License along
//  with the Computational Morphometry Toolkit.  If not, see
//  <http://www.gnu.org/licenses/>.
//
//  $Revision: 4039 $
//
//  $LastChangedDate: 2012-03-16 13:36:14 -0700 (Fri, 16 Mar 2012) $
//
//  $LastChangedBy: torstenrohlfing $
//
*/

#include <System/cmtkStackBacktrace.h>
namespace cmtk { static StackBacktrace StackBacktraceInstance; }

// deliberately crash to test stack trace output
int
testStackBacktrace()
{
  cmtk::StackBacktrace::SetExitCode( 0 );
  char* nullPtr = NULL;
  (*nullPtr) = 0;

  // if we didn't catch a SEGFAULT, the test failed
  return 1;
}

